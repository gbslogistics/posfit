<?php

namespace GbsLogistics\PosFit\FrontendBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('GbsLogisticsPosFitFrontendBundle::index.html.twig');
    }

    public function staticAction($filename)
    {
        if ('dev' !== $this->container->getParameter('kernel.environment')) {
            throw new BadRequestHttpException();
        }

        $file = $this->container->getParameter('kernel.root_dir') . $filename;
        if (is_file($file)) {
            return new Response(file_get_contents($file));
        } else {
            throw new NotFoundHttpException();
        }
    }

    public function testAction()
    {
        return $this->render('GbsLogisticsPosFitFrontendBundle::test.html.twig', [
            'testFiles' => $this->getTestFiles()
        ]);
    }

    /**
     * @return array
     */
    private function getTestFiles()
    {
        $files = [];
        $path = __DIR__ . '/../Resources/public/js/test/';

        if ($dir = opendir($path)) {
            while (false !== $file = readdir($dir)) {
                if (is_file($path . '/' . $file)) {
                    $files[] = $file;
                }
            }
        }
        closedir($dir);

        return $files;
    }
}