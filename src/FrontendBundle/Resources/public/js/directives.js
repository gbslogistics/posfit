angular.module("posfitDirectives", [ "RecursionHelper", "posfitServices" ])

    .directive("posfitModuleGroup", [ "RecursionHelper",
        function (RecursionHelper) {
            "use strict";
            return {
                restrict: "E",
                templateUrl: "/partials/module-group.html",
                scope: {
                    groupScaffold: "="
                },
                compile: function (element) {
                    return RecursionHelper.compile(element);
                },
                controller: function ($scope) {
                    $scope.toggled = {};
                    $scope.entryCount = Object.keys($scope.groupScaffold.childGroups).length
                        + Object.keys($scope.groupScaffold.invTypes).length
                        + Object.keys($scope.groupScaffold.invTypeCollections).length;
                }
            };
        }])

    .directive("posfitInvTypeCollection", [ "RecursionHelper",
        function (RecursionHelper) {
            "use strict";
            return {
                restrict: "E",
                templateUrl: "/partials/inv-type-collection.html",
                compile: function (element) {
                    return RecursionHelper.compile(element);
                },
                scope: {
                    collection: "=",
                    suppressHeader: "="
                },
                controller: function ($scope) {
                    $scope.toggled = false;
                }
            };
        }])

    .directive("posfitSelectInvType", [ "RecursionHelper", "mainDataTerminalFactory", "currentTowerData", "exceptionModal",
        function (RecursionHelper, MainDataTerminalFactory, currentTowerData, exceptionModal) {
            "use strict";
            return {
                restrict: "E",
                templateUrl: "/partials/inv-type-select.html",
                scope: {
                    invTypeScaffolding: "="
                },
                compile: function (element) {
                    return RecursionHelper.compile(element);
                },
                controller: function ($scope) {
                    MainDataTerminalFactory.getInstance().then(function (terminal) {
                        $scope.invType = terminal.getInvType($scope.invTypeScaffolding.typeId);
                        $scope.getAttribute = terminal.getAttribute;
                        $scope.selectModule = function (typeId) {
                            var invType = terminal.getInvType(typeId);

                            if (null !== invType) {
                                try {
                                    currentTowerData.addModule(invType);
                                } catch (ex) {
                                    exceptionModal.open(ex);
                                }
                            }
                        };
                        $scope.mouseOverHandler = function (typeId) {
                            currentTowerData.setMouseOverTypeId(typeId);
                        };
                        $scope.mouseOutHandler = function (typeId) {
                            if (typeId === currentTowerData.mouseOverTypeId) {
                                currentTowerData.setMouseOverTypeId(null);
                            }
                        };
                    });

                    if (undefined !== $scope.invTypeScaffolding.children) {
                        $scope.hasFaction = 'faction' === $scope.invTypeScaffolding.children.subType;
                    } else {
                        $scope.hasFaction = false;
                    }
                }
            };
        }])

    .directive("posfitControlTowerAttributes", [ "RecursionHelper", "mainDataTerminalFactory",
        function (RecursionHelper, mainDataTerminalFactory) {
            "use strict";
            var DisplayAttribute = function (value, unit, icon, name, parentValue) {
                this.value = value;
                this.unit = unit;
                this.icon = icon;
                this.name = name;
                this.parentValue = parentValue;

                this.isString = function () {
                    return "string" === typeof this.value;
                };
            }, formatter = new TwitterCldr.ShortDecimalFormatter(),
                shieldResonanceConversion = function (value) {
                    return 100 * (1 - value);
                };

            return {
                restrict: "A",
                templateUrl: "/partials/control-tower-attributes.html",
                scope: {
                    typeId: "=",
                    parentTypeId: "="
                },
                compile: function (element) {
                    return RecursionHelper.compile(element);
                },
                controller: function ($scope) {
                    mainDataTerminalFactory.getInstance().then(function (terminal) {
                        var invType = terminal.getInvType($scope.typeId),
                            parentType = terminal.getInvType($scope.parentTypeId);
                        $scope.attributes = [];

                        angular.forEach({
                            cpuOutput: null,
                            powerOutput: null,
                            shieldCapacity: function (value) {
                                return formatter.format(parseInt(value, 10));
                            },
                            shieldEmDamageResonance: shieldResonanceConversion,
                            shieldExplosiveDamageResonance: shieldResonanceConversion,
                            shieldKineticDamageResonance: shieldResonanceConversion,
                            shieldThermalDamageResonance: shieldResonanceConversion
                        }, function (valueConversionFunction, attributeName) {
                            var attribute = terminal.getAttribute(attributeName),
                                value = invType.attributeDigest[attributeName],
                                parentValue = null !== parentType ? parentType.attributeDigest[attributeName] : null,
                                unit = "",
                                icon = null;

                            if (null !== attribute) {
                                if (null !== valueConversionFunction) {
                                    value = valueConversionFunction(value);

                                    if (null !== parentValue) {
                                        parentValue = valueConversionFunction(parentValue);
                                    }
                                }

                                if (attribute.hasOwnProperty("unit")) {
                                    unit = attribute.unit.displayName;
                                    if ("%" !== unit) {
                                        unit = " " + unit;
                                    }
                                }

                                if (attribute.hasOwnProperty("icon")) {
                                    icon = attribute.icon.iconFile;
                                }

                                $scope.attributes.push(new DisplayAttribute(value, unit, icon, attribute.displayName.trim(), parentValue));
                            }
                        });

                        $scope.controlTowerResources = terminal.getControlTowerResources($scope.typeId);
                        $scope.isCaldari = 1 === invType.raceID;
                        $scope.isMinmatar = 2 === invType.raceID;
                        $scope.isAmarr = 4 === invType.raceID;
                        $scope.isGallente = 8 === invType.raceID;
                    });
                }
            };
        }])

    .directive("posfitControlTowerSelect", [
        function () {
            "use strict";

            return {
                restrict: "E",
                templateUrl: "/partials/control-tower-single.html",
                scope: {
                    invType: "="
                }
            };
        }])

    .directive("posfitControlTowerSummary", [
        function () {
            "use strict";

            return {
                restrict: "E",
                templateUrl: "/partials/control-tower-summary.html",
                scope: {
                    invType: "=",
                    parentInvType: "="
                }
            };
        }])

    .directive("posfitProgressBar", [ "mainDataTerminalFactory", "currentTowerData",
        function (mainDataTerminalFactory, currentTowerData) {
            "use strict";

            return {
                restrict: "E",
                templateUrl: "/partials/progress-bar.html",
                scope: {
                    tower: "=",
                    amount: "@",
                    max: "=",
                    unit: "=",
                    color: "@"
                },
                controller: function ($scope) {
                    function zeroMouseOverAttributes() {
                        $scope.mouseOverType = null;
                        $scope.mouseOverPercentage = 0;
                        $scope.mouseOverAddendum = 0;
                        $scope.mainBarAugment = 0;
                    }

                    var mouseOverCallback = function (mouseOverTypeId, multiplier) {
                        if (undefined === multiplier) {
                            multiplier = 1;
                        }

                        mainDataTerminalFactory.getInstance().then(function (terminal) {
                            var invType = terminal.getInvType(mouseOverTypeId),
                                amount,
                                amountToAdd;

                            $scope.addendumClass = "progress-bar-warning";
                            if (null !== invType) {
                                $scope.mouseOverType = invType;

                                amount = currentTowerData.getTower()[$scope.amount];
                                amountToAdd = invType.attributeDigest[$scope.amount] * multiplier;

                                if (amountToAdd < 0) {
                                    $scope.mainBarAugment = Math.max(-amount, amountToAdd);
                                    $scope.addendumClass = "";
                                } else if (amount + amountToAdd > $scope.max) {
                                    amountToAdd = $scope.max - amount;
                                    $scope.addendumClass = "progress-bar-danger";
                                }

                                $scope.mouseOverPercentage = 100 * Math.abs(amountToAdd) / $scope.max;
                                $scope.mouseOverAddendum = amountToAdd;
                            } else {
                                zeroMouseOverAttributes();
                            }
                        });
                    };

                    zeroMouseOverAttributes();
                    $scope.colorClass = $scope.color;
                    $scope.getPercentage = function (dividend, divisor) {
                        var result = 100 * dividend / divisor;

                        if (result > 100) {
                            $scope.colorClass = 'progress-bar-danger';
                        } else {
                            $scope.colorClass = $scope.color;
                        }

                        return result;
                    };

                    currentTowerData.attachMouseOverCallback(mouseOverCallback);
                    $scope.$on("$destroy", function () {
                        currentTowerData.detachMouseOverCallbacks();
                    });
                }
            };
        }]);
