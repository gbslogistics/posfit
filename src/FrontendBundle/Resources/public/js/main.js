var posfitApp = angular.module('posfitApp', [
    "ngRoute",
    "posfitControllers",
    "posfitDirectives",
    "gbsFilters",
    "ui.bootstrap",
    "focusOn",
    "ngClipboard"
]);

posfitApp.config(['$routeProvider', 'ngClipProvider',
    function ($routeProvider, ngClipProvider) {
        "use strict";

        $routeProvider.when('/', {
            templateUrl: '/partials/tower-select.html'
        }).when('/edit', {
            templateUrl: '/partials/edit.html'
        }).when('/legal', {
            templateUrl: '/partials/legal.html'
        }).when("/tower/:towerString", {
            templateUrl: '/partials/view.html'
        }).otherwise({
            redirectTo: '/'
        });

        ngClipProvider.setPath("//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.1.6/ZeroClipboard.swf");
        ngClipProvider.setConfig({ zIndex: 50 });
    }]);
