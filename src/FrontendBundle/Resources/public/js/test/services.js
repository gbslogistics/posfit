describe("Stacking Penalty Test Suite: ", function () {
    "use strict";
    var shieldResonanceCalculator, stackingPenaltyFactor;

    beforeEach(module("posfitServices"));

    beforeEach(function () {
        //noinspection JSLint
        inject(function (_shieldResonanceCalculator_, _stackingPenaltyFactor_) {
            //noinspection JSLint
            shieldResonanceCalculator = _shieldResonanceCalculator_;
            //noinspection JSLint
            stackingPenaltyFactor = _stackingPenaltyFactor_;
        });
    });

    describe("The stacking penalty factor service", function () {
        it("should have a getFactor function", function () {
            expect(typeof stackingPenaltyFactor.getFactor).toEqual("function");
        });

        describe("when invoking the getFactor function", function () {
            Number.prototype.round = function (places) {
                return +(Math.round(this + "e+" + places)  + "e-" + places);
            };

            it("should return 100% with a count of 0", function () {
                expect(stackingPenaltyFactor.getFactor(0)).toBe(1);
            });

            it("should return 86.9% with a count of 1", function () {
                expect(stackingPenaltyFactor.getFactor(1).round(3)).toBe(0.869);
            });

            it("should return 57.1% with a count of 2", function () {
                expect(stackingPenaltyFactor.getFactor(2).round(3)).toBe(0.571);
            });

            it("should return 28.3% with a count of 3", function () {
                expect(stackingPenaltyFactor.getFactor(3).round(3)).toBe(0.283);
            });

            it("should return 10.6% with a count of 4", function () {
                expect(stackingPenaltyFactor.getFactor(4).round(3)).toBe(0.106);
            });

            it("should return 3.0% with a count of 5", function () {
                expect(stackingPenaltyFactor.getFactor(5).round(3)).toBe(0.030);
            });
        });
    });

    describe("The shield resonance calculator", function () {
        it("should have a calculate function", function () {
            expect(typeof shieldResonanceCalculator.calculate).toEqual("function");
        });

        describe("when invoking the calculate function", function () {
            it("should return the initial attribute value if there are no modifiers", function () {
                var initialAttributeValue = 0.5,
                    finalAttributeValue = shieldResonanceCalculator.calculate(initialAttributeValue, []);

                expect(finalAttributeValue).toEqual(initialAttributeValue);
            });

            it("should not penalize the first modifier", function () {
                var initialAttributeValue = 0.6,
                    finalAttributeValue = shieldResonanceCalculator.calculate(initialAttributeValue, [ 0.65 ]);

                expect(finalAttributeValue).toEqual(0.39);
            });

            it("should penalize the second modifier", function () {
                var initialAttributeValue = 0.6,
                    finalAttributeValue = shieldResonanceCalculator.calculate(initialAttributeValue, [ 0.65, 0.65 ]);

                expect(finalAttributeValue.round(4)).toEqual(0.2714);

            });

            it("should penalize the third modifier", function () {
                var initialAttributeValue = 0.6,
                    finalAttributeValue = shieldResonanceCalculator.calculate(initialAttributeValue, [ 0.65, 0.65, 0.65 ]);

                expect(finalAttributeValue.round(4)).toEqual(0.2172);

            });
        });
    });
});

describe("Tower Data Serializer Test Suite:", function () {
    "use strict";
    var protoBufBuilderFactory, towerDataSerializer;

    beforeEach(function () {
        module("posfitServices", function ($provide) {
            $provide.value('$q', Q);
        });
    });

    //noinspection JSLint
    beforeEach(inject(function (_protoBufBuilderFactory_, _towerDataSerializer_) {
        //noinspection JSLint
        protoBufBuilderFactory = _protoBufBuilderFactory_;
        //noinspection JSLint
        towerDataSerializer = _towerDataSerializer_;
    }));

    describe("The protoBufBuilderFactory service", function () {
        it("should return a tower definition and not throw", function (done) {
            protoBufBuilderFactory.getInstance().then(function (TowerDefinition) {
                expect(TowerDefinition).toBeDefined();
                done();
            }).catch(function (err) {
                throw err;
            });

        });

        it("should contain schematics for Tower and associated Module", function (done) {
            protoBufBuilderFactory.getInstance().then(function (TowerDefinition) {
                expect(TowerDefinition.Tower).toBeDefined();
                expect(TowerDefinition.Tower.Module).toBeDefined();
                done();
            });
        });
    });

    describe("The towerDataSerializer service", function () {
        var tower = {
            invType: {
                typeID: 1234
            },
            getModules: function () {
                return [
                    {
                        invType: {
                            typeID: 567
                        },
                        quantity: 2
                    }
                ];
            }
        };

        it("should serialize a tower to base64", function (done) {
            towerDataSerializer.serialize(tower).then(function (base64) {
                expect(typeof base64).toBe("string");
                expect(base64.length).toBeGreaterThan(0);
                expect(base64).toBe("CIOyAxDSCRoFCLcEEAI=");
                done();
            });
        });

        it("should be able to deserialize a thing that was serialized", function (done) {
            towerDataSerializer.serialize(tower).then(function (base64) {
                towerDataSerializer.deserialize(base64).then(function (reserializedTower) {
                    var firstModule, expectedFirstModule = tower.getModules()[0];

                    expect(reserializedTower).toBeDefined();
                    expect(typeof reserializedTower).toBe("object");
                    expect(reserializedTower.type_id).toEqual(tower.invType.typeID);
                    expect(reserializedTower.modules.length).toEqual(tower.getModules().length);

                    firstModule = reserializedTower.modules[0];
                    expect(firstModule.type_id).toEqual(expectedFirstModule.invType.typeID);
                    expect(firstModule.quantity).toEqual(expectedFirstModule.quantity);
                    done();
                });
            });
        });
    });
});

describe("Tower Data Validator Test Suite:", function () {
    "use strict";
    var towerDataValidator,
        controlTowerGroupId,
        towerInvType = 123,
        moduleInvType = 456,
        notFoundInvType = 789,
        mockTerminal = {
            getInvType: function (invType) {
                switch (invType) {
                case towerInvType:
                    return {
                        groupID: controlTowerGroupId
                    };
                case moduleInvType:
                    return {
                        groupID: 912
                    };
                default:
                    return null;
                }
            }
        };

    beforeEach(function () {
        module("posfitServices", function ($provide) {
            $provide.value('$q', Q);
            $provide.value('mainDataTerminalFactory', {
                getInstance: function () {
                    var deferred = Q.defer();
                    deferred.resolve(mockTerminal);

                    return deferred.promise;
                }
            });
        });
    });

    //noinspection JSLint
    beforeEach(inject(function (_towerDataValidator_, _controlTowerGroupId_) {
        //noinspection JSLint
        towerDataValidator = _towerDataValidator_;
        //noinspection JSLint
        controlTowerGroupId = _controlTowerGroupId_;
    }));

    describe("The tower data validator service", function () {
        it("should exist and have a validate function", function () {
            expect(towerDataValidator).toBeDefined();
            expect(typeof towerDataValidator).toBe("object");
            expect(towerDataValidator.validate).toBeDefined();
            expect(typeof towerDataValidator.validate).toBe("function");
        });

        describe("when invoking the validate function", function () {
            function buildMockData(towerTypeId, modules, magicNumber) {
                if (undefined === magicNumber) {
                    magicNumber = 55555;
                }

                return {
                    magic_value: magicNumber,
                    type_id: towerTypeId,
                    modules: modules
                };
            }

            function buildMockModule(typeId, quantity) {
                return {
                    type_id: typeId,
                    quantity: quantity
                };
            }

            function expectErrorMessage(err, expectedMessage, done) {
                expect(err).toBeDefined();
                expect(err.length).toBeGreaterThan(0);
                expect(err).toEqual(expectedMessage);
                done();
            }

            function validateTower(tower, expectedErrorMessage, done) {
                towerDataValidator.validate(tower)
                    .then(function () {
                        expect("").toEqual(expectedErrorMessage);
                        done();
                    }, function (err) {
                        expectErrorMessage(err, expectedErrorMessage, done);
                    });
            }

            it("should fail when no magic number exists", function (done) {
                validateTower({}, towerDataValidator.invalidMagicValueError, done);
            });

            it("should fail when the magic number is not correct", function (done) {
                validateTower(buildMockData(null, null, 12345), towerDataValidator.invalidMagicValueError, done);
            });

            it("should fail if tower inv type does not exist", function (done) {
                validateTower(
                    buildMockData(notFoundInvType),
                    towerDataValidator.missingInvTypeError.replace(/\$typeId/, notFoundInvType),
                    done
                );
            });

            it("should fail if tower inv type is not a control tower", function (done) {
                validateTower(
                    buildMockData(moduleInvType),
                    towerDataValidator.invalidTowerInvTypeGroupError,
                    done
                );
            });

            it("should fail if a module's inv type does not exist", function (done) {
                validateTower(
                    buildMockData(towerInvType, [ buildMockModule(notFoundInvType, 1) ]),
                    towerDataValidator.missingInvTypeError.replace(/\$typeId/, notFoundInvType),
                    done
                );
            });

            it("should fail if a module is a control tower", function (done) {
                validateTower(
                    buildMockData(towerInvType, [ buildMockModule(towerInvType, 1) ]),
                    towerDataValidator.invalidModuleInvTypeGroupError,
                    done
                );
            });

            it("should fail if module quantity is less than zero", function (done) {
                validateTower(
                    buildMockData(towerInvType, [ buildMockModule(moduleInvType, -1) ]),
                    towerDataValidator.invalidQuantityError,
                    done
                );
            });
        });
    });
});
