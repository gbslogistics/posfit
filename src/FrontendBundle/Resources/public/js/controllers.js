angular.module("posfitControllers", [
    "posfitServices"
])
    .controller("ModuleSelectorCtrl", [ "$scope", "mainDataTerminalFactory", "focus", "controlTowerGroupId",
        function ($scope, mainDataTerminalFactory, focus, controlTowerGroupId) {
            "use strict";

            function escapeRegularExpression(s) {
                return String(s).replace(/([\-()\[\]{}+?*.$\^|,:#<!\\])/g, '\\$1').
                    replace(/\x08/g, '\\x08');
            }

            $scope.filteredModules = [];
            $scope.filterInputClass = "";
            $scope.clearFilter = function () {
                $scope.filter = "";
                $scope.filteredModules = [];
                $scope.filterInputClass = "";
                focus("module-filter");
            };

            mainDataTerminalFactory.getInstance().then(function (terminal) {
                $scope.scaffolding = terminal.getScaffolding();
                $scope.applyFilter = function () {
                    var filteredModules = [], regexp;

                    if ($scope.filter.length > 2) {
                        regexp = new RegExp(escapeRegularExpression($scope.filter), 'i');
                        terminal.forEachInvType(function (invType) {
                            if (regexp.test(invType.typeName) && invType.groupID !== controlTowerGroupId) {
                                filteredModules.push(invType);
                            }
                        });

                        filteredModules.sort(function (a, b) {
                            return a.typeName.localeCompare(b.typeName);
                        });

                        if (0 === filteredModules.length) {
                            $scope.filterInputClass = "has-error";
                        } else {
                            $scope.filterInputClass = "has-success";
                        }
                    } else {
                        $scope.filterInputClass = "";
                    }

                    $scope.filteredModules = filteredModules;
                };
            });
        }])

    .controller("CurrentTowerInfoCtrl", [ "$scope", "$location", "currentTowerData", "mainDataTerminalFactory",
        function ($scope, $location, currentTowerData, mainDataTerminalFactory) {
            "use strict";

            $scope.changeTowerType = function () {
                $location.path("/");
            };

            mainDataTerminalFactory.getInstance().then(function (terminal) {
                var tower = currentTowerData.getTower();

                if (null === tower) {
                    $location.path("/new");
                } else {
                    $scope.typeId = tower.invType.typeID;
                    $scope.typeName = tower.invType.typeName;
                    $scope.getAttribute = terminal.getAttribute;
                    $scope.tower = tower;
                    $scope.maxCpu = tower.getMaxCpu();
                    $scope.maxGrid = tower.getMaxPower();
                    $scope.readOnly = currentTowerData.readOnly;
                }
            });
        }])

    .controller("CurrentTowerShieldCtrl", [ "$scope", "shieldResonanceCalculator", "currentTowerData", "mainDataTerminalFactory",
        function ($scope, shieldResonanceCalculator, currentTowerData, mainDataTerminalFactory) {
            "use strict";

            var Resonance = function (towerAttributeName, moduleAttributeName, decoratorClass) {
                this.towerAttributeName = towerAttributeName;
                this.moduleAttributeName = moduleAttributeName;
                this.decoratorClass = decoratorClass;
            }, DamagePattern = function (name, decoratorClass, em, exp, kin, therm) {
                this.name = name;
                this.decoratorClass = decoratorClass;
                this.shieldEmDamageResonance = em;
                this.shieldExplosiveDamageResonance = exp;
                this.shieldKineticDamageResonance = kin;
                this.shieldThermalDamageResonance = therm;
            }, omniPattern = new DamagePattern("Omni", "text-success", 0.25, 0.25, 0.25, 0.25);

            function recalculateEhp() {
                var shieldAmount = $scope.tower.invType.attributeDigest.shieldCapacity;

                $scope.ehp = 0;
                angular.forEach($scope.resonances, function (resonance) {
                    var resonanceValue = shieldResonanceCalculator.calculate(
                        $scope.tower[resonance.towerAttributeName],
                        $scope.tower[resonance.moduleAttributeName]
                    ), multiplier = $scope.damagePattern[resonance.towerAttributeName],
                        affectedShieldSlice;

                    affectedShieldSlice = shieldAmount * multiplier;
                    $scope.ehp += affectedShieldSlice / resonanceValue;
                });
            }

            $scope.resonances = [
                new Resonance("shieldEmDamageResonance", "emDamageResonanceMultiplier", "progress-bar-info"),
                new Resonance("shieldExplosiveDamageResonance", "explosiveDamageResonanceMultiplier", "progress-bar-warning"),
                new Resonance("shieldKineticDamageResonance", "kineticDamageResonanceMultiplier", ""),
                new Resonance("shieldThermalDamageResonance", "thermalDamageResonanceMultiplier", "progress-bar-danger")
            ];
            $scope.damagePatterns = [
                omniPattern,
                new DamagePattern("EM", "text-info", 1, 0, 0, 0),
                new DamagePattern("Explosive", "text-warning", 0, 1, 0, 0),
                new DamagePattern("Kinetic", "text-muted", 0, 0, 1, 0),
                new DamagePattern("Thermal", "text-danger", 0, 0, 0, 1)
            ];
            $scope.resonanceValues = {
                shieldEmDamageResonance: null,
                shieldExplosiveDamageResonance: null,
                shieldKineticDamageResonance: null,
                shieldThermalDamageResonance: null
            };
            $scope.ehp = 0;
            $scope.damagePattern = omniPattern;
            $scope.tower = currentTowerData.getTower();
            $scope.changeDamagePattern = function (pattern) {
                $scope.damagePattern = pattern;
                recalculateEhp();
            };

            $scope.$watchCollection('tower', function () {
                if ($scope.tower && $scope.tower.hasOwnProperty && $scope.tower.hasOwnProperty("recalculate")) {
                    $scope.tower.recalculate();
                    angular.forEach($scope.resonances, function (resonance) {
                        var resonanceValue = shieldResonanceCalculator.calculate(
                            $scope.tower[resonance.towerAttributeName],
                            $scope.tower[resonance.moduleAttributeName]
                        );

                        $scope.resonanceValues[resonance.towerAttributeName] = 100 * (1 - resonanceValue);
                    });
                    recalculateEhp();
                }
            });

            mainDataTerminalFactory.getInstance().then(function (terminal) {
                $scope.getAttribute = terminal.getAttribute;
            });
        }])

    .controller("CurrentTowerModulesCtrl", [ "$scope", "currentTowerData", "mainDataTerminalFactory", "exceptionModal",
        function ($scope, currentTowerData, mainDataTerminalFactory, exceptionModal) {
            "use strict";

            mainDataTerminalFactory.getInstance().then(function () {
                var uri = window.location.href.replace(window.location.hash, '');

                $scope.modules = currentTowerData.getModules();
                $scope.currentTowerData = currentTowerData;
                $scope.incrementModule = function (module) {
                    try {
                        currentTowerData.addModule(module.getInvType());
                    } catch (ex) {
                        exceptionModal.open(ex);
                    }
                };
                $scope.decrementModule = function (module) {
                    var typeId = module.getInvType().typeID;
                    currentTowerData.decrementModule(typeId);
                    if (module.getQuantity() < 1) {
                        $scope.mouseOutHandler(typeId);
                    }
                };
                $scope.removeModule = function (module) {
                    var typeId = module.getInvType().typeID;
                    currentTowerData.removeModule(typeId);
                    $scope.mouseOutHandler(typeId);
                };

                $scope.mouseOverHandler = function (typeId, multiplier) {
                    currentTowerData.setMouseOverTypeId(typeId, multiplier);
                };
                $scope.mouseOutHandler = function (typeId) {
                    if (typeId === currentTowerData.mouseOverTypeId) {
                        currentTowerData.setMouseOverTypeId(null);
                    }
                };

                $scope.$watchCollection(function () { return currentTowerData.serializedTowerString; }, function () {
                    $scope.url = uri + "#/tower/" + currentTowerData.serializedTowerString;
                });
            });
        }])

    .controller("ControlTowerSelectCtrl", [ "$scope", "$location", "mainDataTerminalFactory", "currentTowerData",
        function ($scope, $location, mainDataTerminalFactory, currentTowerData) {
            "use strict";

            mainDataTerminalFactory.getInstance().then(function (terminal) {
                $scope.controlTowers = terminal.getControlTowers().invTypeCollections;
                $scope.factionControlTowers = terminal.getFactionControlTowers();
                $scope.getInvType = terminal.getInvType;
                $scope.selectedTypeId = null;
                $scope.selectedParentInvTypeId = null;
                $scope.previewControlTower = function (typeId) {
                    $scope.selectedTypeId = typeId;
                    if (typeId !== $scope.selectedParentInvTypeId) {
                        $scope.selectedParentInvTypeId = null;
                    }
                };

                $scope.previewFactionControlTower = function (typeId) {
                    $scope.selectedTypeId = typeId;
                };

                $scope.displayFactionControlTowers = function (typeId) {
                    $scope.selectedParentInvTypeId = typeId;
                };

                $scope.clearControlTower = function () {
                    $scope.selectedTypeId = null;
                    $scope.selectedParentInvTypeId = null;
                };

                $scope.selectTower = function (typeId) {
                    var invType = terminal.getInvType(typeId);

                    if (null === currentTowerData.getTower()) {
                        currentTowerData.newTower(invType);
                    } else {
                        currentTowerData.changeTowerType(invType);
                    }

                    $location.path('/edit');
                };
            });
        }])

    .controller("ExceptionModalCtrl", [ "$scope", "$modalInstance", "message",
        function ($scope, $modalInstance, message) {
            "use strict";

            $scope.message = "string" === typeof message ? message : angular.toJson(message);
            $scope.ok = function () {
                $modalInstance.close();
            };
        }])

    .controller("NavBarCtrl", [ "$scope", "$location", "currentTowerData",
        function ($scope, $location, currentTowerData) {
            "use strict";

            $scope.newTower = function () {
                currentTowerData.clearTower();
                $location.path("/new");
            };
        }])

    .controller("LegalCtrl", [ "$scope", "currentTowerData",
        function ($scope, currentTowerData) {
            "use strict";

            $scope.tower = currentTowerData.getTower();
        }])

    .controller("ViewCtrl", [
        "$scope", "$routeParams", "towerDataSerializer", "towerDataValidator", "currentTowerData", "towerDataDenormalizer", "$location",
        function ($scope, $routeParams, towerDataSerializer, towerDataValidator, currentTowerData, towerDataDenormalizer, $location) {
            "use strict";

            $scope.error = null;
            $scope.currentTowerData = currentTowerData;
            $scope.editTower = function () {
                currentTowerData.readOnly = false;
                $location.path("/edit");
            };

            towerDataSerializer
                .deserialize($routeParams.towerString)
                .then(function (data) { return towerDataValidator.validate(data); })
                .then(function (data) {
                    currentTowerData.readOnly = true;
                    towerDataDenormalizer.denormalize(data);
                })
                .catch(function (err) {
                    $scope.error = err;
                });
        }]);
