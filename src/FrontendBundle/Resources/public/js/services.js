(function () {
    "use strict";

    var shieldResonanceMap = {
            shieldEmDamageResonance: "emDamageResonanceMultiplier",
            shieldExplosiveDamageResonance: "explosiveDamageResonanceMultiplier",
            shieldKineticDamageResonance: "kineticDamageResonanceMultiplier",
            shieldThermalDamageResonance: "thermalDamageResonanceMultiplier"
        },
        MainDataTerminal = function (data) {
            var scaffolding = data.scaffolding,
                invTypes = data.invTypes,
                groups = data.groups,
                attributes = data.attributes,
                controlTowers = data.controlTowers,
                controlTowerResources = data.controlTowerResources;

            this.getInvType = function (typeId) {
                return invTypes.hasOwnProperty(typeId) ? invTypes[typeId] : null;
            };

            this.getGroup = function (groupId) {
                return groups.hasOwnProperty(groupId) ? groups[groupId] : null;
            };

            this.getAttribute = function (attributeId) {
                return attributes.hasOwnProperty(attributeId) ? attributes[attributeId] : null;
            };

            this.getScaffolding = function () {
                return scaffolding;
            };

            this.getControlTowers = function () {
                return controlTowers;
            };

            this.getControlTowerResources = function (controlTowerTypeId) {
                if (controlTowerResources.hasOwnProperty(controlTowerTypeId)) {
                    return controlTowerResources[controlTowerTypeId];
                }

                return null;
            };

            this.getFactionControlTowers = function () {
                var factionControlTowers = {};

                angular.forEach(controlTowers.invTypeCollections, function (collection) {
                    angular.forEach(collection.invTypes, function (scaffolding) {
                        var typeId = scaffolding.typeId,
                            factionTypeIds = [];

                        angular.forEach(scaffolding.children.invTypes, function (childType) {
                            factionTypeIds.push(childType.typeId);
                        });
                        factionControlTowers[typeId] = factionTypeIds;
                    });
                });

                return factionControlTowers;
            };

            this.forEachInvType = function (callback) {
                angular.forEach(invTypes, callback);
            };
        },
        Module = function (invType, quantity) {
            this.quantity = undefined === quantity ? 1 : quantity;
            this.invType = invType;

            this.increment = function (addedQuantity) {
                if (undefined === addedQuantity) {
                    addedQuantity = 1;
                }
                this.quantity += addedQuantity;
                return true;
            };

            this.decrement = function () {
                if (this.quantity > 0) {
                    this.quantity -= 1;
                }
            };

            this.getQuantity = function () {
                return this.quantity;
            };

            this.getInvType = function () {
                return this.invType;
            };
        },
        Tower = function (invType) {
            this.invType = invType;
            this.modules = {};
            this.cpu = 0;
            this.power = 0;
            this.moduleCount = 0;
            this.shieldEmDamageResonance = invType.attributeDigest.shieldEmDamageResonance;
            this.shieldExplosiveDamageResonance = invType.attributeDigest.shieldExplosiveDamageResonance;
            this.shieldKineticDamageResonance = invType.attributeDigest.shieldKineticDamageResonance;
            this.shieldThermalDamageResonance = invType.attributeDigest.shieldThermalDamageResonance;
            this.emDamageResonanceMultiplier = [];
            this.explosiveDamageResonanceMultiplier = [];
            this.kineticDamageResonanceMultiplier = [];
            this.thermalDamageResonanceMultiplier = [];

            this.recalculate = function () {
                var self = this;

                angular.forEach(shieldResonanceMap, function (moduleAttributeName) {
                    self[moduleAttributeName].length = 0;
                });
                self.cpu = 0;
                self.power = 0;

                angular.forEach(this.modules, function (module) {
                    var moduleInvType = module.getInvType();

                    self.cpu += moduleInvType.attributeDigest.cpu * module.getQuantity();
                    self.power += moduleInvType.attributeDigest.power * module.getQuantity();

                    angular.forEach(shieldResonanceMap, function (moduleAttributeName) {
                        var multiplier = module.getInvType().attributeDigest.hasOwnProperty(moduleAttributeName)
                            ? module.getInvType().attributeDigest[moduleAttributeName]
                            : 1,
                            i;

                        if (1 === multiplier) {
                            return;
                        }

                        for (i = 0; i < module.getQuantity(); i += 1) {
                            self[moduleAttributeName].push(multiplier);
                        }
                    });
                });
            };

            this.getModules = function () {
                return this.modules;
            };

            this.getMaxCpu = function () { return invType.attributeDigest.cpuOutput; };
            this.getMaxPower = function () { return invType.attributeDigest.powerOutput; };

            this.addModule = function (moduleInvType, addedQuantity) {
                var key = moduleInvType.typeID, quantity, maxPerSolarSystem;

                if (undefined === addedQuantity) {
                    addedQuantity = 1;
                }

                if (this.modules.hasOwnProperty(key)) {
                    quantity = this.modules[key].getQuantity();
                    if (moduleInvType.attributeDigest.hasOwnProperty("posAnchoredPerSolarSystemAmount")) {
                        maxPerSolarSystem = moduleInvType.attributeDigest.posAnchoredPerSolarSystemAmount;
                        if (quantity >= maxPerSolarSystem) {
                            throw "There cannot be more than " + maxPerSolarSystem.toString() + " module"
                                + (1 === maxPerSolarSystem ? "" : "s") + " of type " + moduleInvType.typeName
                                + " on a control tower at once.";
                        }
                    }

                    this.modules[key].increment(addedQuantity);
                } else {
                    this.modules[key] = new Module(moduleInvType, addedQuantity);
                }
                this.recalculate();
            };

            this.decrementModule = function (typeId) {
                var module;
                if (this.modules.hasOwnProperty(typeId)) {
                    module = this.modules[typeId];
                    module.decrement();

                    if (module.getQuantity() < 1) {
                        this.removeModule(typeId);
                    }

                    this.recalculate();
                }
            };

            this.removeModule = function (typeId) {
                if (this.modules.hasOwnProperty(typeId)) {
                    delete this.modules[typeId];
                }
                this.recalculate();
            };

            this.copyModulesFromTower = function (tower) {
                this.modules = tower.getModules();
            };
        };


    angular.module("posfitServices", [
        "angular-data.DSCacheFactory"
    ])
        .constant("controlTowerGroupId", 365)

        .factory("mainDataLoader", [ "$http", "$q", "DSCacheFactory",
            function ($http, $q, DSCacheFactory) {
                var cache = new DSCacheFactory("mainDataLoader", {
                    storageMode: "localStorage",
                    storagePrefix: "posfit.mainDataLoader.",
                    // One day
                    maxAge: 86400000
                });

                return function () {
                    var deferred = $q.defer();

                    $http.get(Routing.generate("posfit_api_data"), { cache: cache })
                        .success(function (data) {
                            deferred.resolve(data);
                        }).error(function (data, status) {
                            deferred.reject({ data: data, status: status});
                        });

                    return deferred.promise;
                };
            }])

        .factory("mainDataTerminalFactory", [ "mainDataLoader", "$q",
            function (mainDataLoader, $q) {
                var terminalInstance = null, deferred = $q.defer();

                return {
                    getInstance: function () {
                        if (null !== terminalInstance) {
                            deferred.resolve(terminalInstance);
                        } else {
                            mainDataLoader().then(function (data) {
                                terminalInstance = new MainDataTerminal(data);

                                deferred.resolve(terminalInstance);
                            }, function () {
                                deferred.reject("Cannot load data into terminal.");
                            });
                        }

                        return deferred.promise;
                    }
                };
            }])

        .service("currentTowerData", [ "towerDataSerializer",
            function (towerDataSerializer) {
                var tower = null,
                    mouseOverCallbacks = new Set(),
                    currentTowerData;

                function serializeTower(tower) {
                    towerDataSerializer.serialize(tower).then(function (base64) {
                        currentTowerData.serializedTowerString = base64;
                    });
                }

                currentTowerData = {
                    mouseOverTypeId: null,
                    serializedTowerString: null,
                    readOnly: false,

                    getTower: function () {
                        return tower;
                    },
                    newTower: function (invType) {
                        tower = new Tower(invType);
                        this.serializedTowerString = serializeTower(tower);

                        return tower;
                    },
                    changeTowerType: function (invType) {
                        var newTower = new Tower(invType);

                        newTower.copyModulesFromTower(tower);
                        newTower.recalculate();
                        tower = newTower;
                        this.readOnly = false;
                        this.serializedTowerString = serializeTower(tower);
                    },
                    clearTower: function () {
                        tower = null;
                        this.readOnly = false;
                        this.serializedTowerString = null;
                        this.detachMouseOverCallbacks();
                    },
                    addModule: function (invType, quantity) {
                        if (undefined === quantity) {
                            quantity = 1;
                        }

                        if (null !== tower) {
                            tower.addModule(invType, quantity);
                            this.runMouseOverCallbacks(this.mouseOverTypeId, 1);
                            this.serializedTowerString = serializeTower(tower);
                        }
                    },
                    decrementModule: function (typeId) {
                        if (null !== tower) {
                            tower.decrementModule(typeId);
                            this.serializedTowerString = serializeTower(tower);
                        }
                    },
                    removeModule: function (typeId) {
                        if (null !== tower) {
                            tower.removeModule(typeId);
                            this.serializedTowerString = serializeTower(tower);
                        }
                    },
                    getModules: function () {
                        if (null !== tower) {
                            return tower.getModules();
                        }
                    },
                    setMouseOverTypeId: function (newTypeId, multiplier) {
                        this.mouseOverTypeId = newTypeId;
                        this.runMouseOverCallbacks(newTypeId, multiplier);
                    },
                    runMouseOverCallbacks: function (newTypeId, multiplier) {
                        mouseOverCallbacks.forEach(function (callback) {
                            callback(newTypeId, multiplier);
                        });
                    },
                    attachMouseOverCallback: function (callback) {
                        mouseOverCallbacks.add(callback);
                    },
                    detachMouseOverCallbacks: function () {
                        mouseOverCallbacks = new Set();
                    }
                };

                return currentTowerData;
            }])

        .service("exceptionModal", [ "$modal",
            function ($modal) {
                return {
                    open: function (message) {
                        $modal.open({
                            templateUrl: "/partials/exception-modal.html",
                            controller: "ExceptionModalCtrl",
                            resolve: {
                                message: function () {
                                    return message;
                                }
                            }
                        });
                    }
                };
            }])

        .service("stackingPenaltyFactor", [
            function () {
                return {
                    getFactor: function (count) {
                        return Math.pow(Math.E, -Math.pow(count / 2.67, 2));
                    }
                };
            }])

        .service("shieldResonanceCalculator", [ "stackingPenaltyFactor",
            function (stackingPenaltyFactor) {
                return {
                    calculate: function (attributeInitialValue, attributeModifiers) {
                        var modifiers = angular.copy(attributeModifiers),
                            finalAttributeValue = attributeInitialValue,
                            i;

                        modifiers.sort(function (a, b) {
                            return a - b;
                        });

                        for (i = 0; i < modifiers.length; i += 1) {
                            modifiers[i] = (1 - modifiers[i]) * stackingPenaltyFactor.getFactor(i);
                        }

                        for (i = 0; i < modifiers.length; i += 1) {
                            finalAttributeValue *= (1 - modifiers[i]);
                        }

                        return finalAttributeValue;
                    }
                };
            }])

        .service("protoBufBuilderFactory", [ "$q",
            function ($q) {
                var ProtoBuf = dcodeIO.ProtoBuf,
                    deferred = $q.defer();

                ProtoBuf.loadProtoFile("/tower.proto", function (err, builder) {
                    if (null === err) {
                        deferred.resolve(builder.build("gbslogistics.posfit"));
                    } else {
                        deferred.reject(err);
                    }
                });

                return {
                    getInstance: function () {
                        return deferred.promise;
                    }
                };
            }])

        .service("towerDataSerializer", [ "$q", "protoBufBuilderFactory",
            function ($q, protoBufBuilderFactory) {

                return {
                    serialize: function (tower) {
                        var deferred = $q.defer();

                        protoBufBuilderFactory.getInstance().then(function (TowerDefinition) {
                            var serializedTower,
                                modules = [];

                            angular.forEach(tower.getModules(), function (module) {
                                var typeId, quantity, moduleObject;

                                typeId = module.invType.typeID;
                                quantity = module.quantity;

                                moduleObject = new TowerDefinition.Tower.Module();
                                moduleObject.type_id = typeId;
                                if (module.quantity > 1) {
                                    moduleObject.quantity = quantity;
                                }

                                modules.push(moduleObject);
                            });

                            serializedTower = new TowerDefinition.Tower();
                            serializedTower.type_id = tower.invType.typeID;
                            serializedTower.modules = modules;

                            deferred.resolve(serializedTower.toBase64());
                        });

                        return deferred.promise;
                    },
                    deserialize: function (data) {
                        var deferred = $q.defer();

                        protoBufBuilderFactory.getInstance().then(function (TowerDefinition) {
                            try {
                                deferred.resolve(TowerDefinition.Tower.decode64(data));
                            } catch (e) {
                                if (e.decoded) {
                                    deferred.resolve(e.decoded);
                                } else {
                                    deferred.reject("Unable to decode data.");
                                }
                            }
                        });

                        return deferred.promise;
                    }
                };
            }])

        .service("towerDataValidator", [ "$q", "mainDataTerminalFactory", "controlTowerGroupId",
            function ($q, mainDataTerminalFactory, controlTowerGroupId) {
                return {
                    missingInvTypeError: "Unable to locate type with ID #$typeId or it is not a type that is involved with building a starbase.",
                    invalidMagicValueError: "Tower data passed is not a valid instance of tower data.",
                    invalidTowerInvTypeGroupError: "Tower type provided is not actually a control tower.",
                    invalidModuleInvTypeGroupError: "Modules attached to a control tower cannot themselves be control towers.",
                    invalidQuantityError: "Quantity for a module must be greater than or equal to zero.",

                    validate: function (data) {
                        var deferred = $q.defer(), self = this;

                        mainDataTerminalFactory.getInstance().then(function (terminal) {
                            var towerType, rejectionReason = null;

                            function getInvType(typeId) {
                                var invType = terminal.getInvType(typeId);

                                if (null === invType) {
                                    rejectionReason = rejectionReason || self.missingInvTypeError.replace(/\$typeId/, typeId);
                                }

                                return invType;
                            }

                            if (!data.hasOwnProperty("magic_value") || 55555 !== data.magic_value) {
                                rejectionReason = rejectionReason || self.invalidMagicValueError;
                            }

                            towerType = getInvType(data.type_id);
                            if (null !== towerType && towerType.groupID !== controlTowerGroupId) {
                                rejectionReason = rejectionReason || self.invalidTowerInvTypeGroupError;
                            }

                            if (data.hasOwnProperty("modules")) {
                                angular.forEach(data.modules, function (module) {
                                    var invType = getInvType(module.type_id);

                                    if (null !== invType) {
                                        if (invType.hasOwnProperty("groupID") && invType.groupID === controlTowerGroupId) {
                                            rejectionReason = rejectionReason || self.invalidModuleInvTypeGroupError;
                                        } else if (module.hasOwnProperty("quantity") && module.quantity < 0) {
                                            rejectionReason = rejectionReason || self.invalidQuantityError;
                                        }
                                    }
                                });
                            }

                            if (rejectionReason) {
                                deferred.reject(rejectionReason);
                            } else {
                                deferred.resolve(data);
                            }
                        });

                        return deferred.promise;
                    }
                };
            }])

        .service("towerDataDenormalizer", [ "currentTowerData", "mainDataTerminalFactory",
            function (currentTowerData, mainDataTerminalFactory) {
                return {
                    denormalize: function (data) {
                        mainDataTerminalFactory.getInstance().then(function (terminal) {
                            var towerInvType = terminal.getInvType(data.type_id);
                            currentTowerData.newTower(towerInvType);
                            angular.forEach(data.modules, function (module) {
                                if (!module.hasOwnProperty("quantity") || null === module.quantity) {
                                    module.quantity = 1;
                                }

                                currentTowerData.addModule(terminal.getInvType(module.type_id), module.quantity);
                            });
                        });
                    }
                };
            }]);
}());
