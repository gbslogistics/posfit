<?php

namespace GbsLogistics\PosFit\ApiBundle;


use GbsLogistics\SdeEntityBundle\Entity\InvType;
use JMS\Serializer\Context;
use JMS\Serializer\Exclusion\ExclusionStrategyInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;

class PosFitExclusionStrategy implements ExclusionStrategyInterface
{
    private static $allowedProperties = null;

    /**
     * Whether the class should be skipped.
     *
     * @param ClassMetadata $metadata
     *
     * @return boolean
     */
    public function shouldSkipClass(ClassMetadata $metadata, Context $context)
    {
        return false;
    }

    /**
     * Whether the property should be skipped.
     *
     * @param PropertyMetadata $property
     *
     * @return boolean
     */
    public function shouldSkipProperty(PropertyMetadata $property, Context $context)
    {
        if (null !== $allowedProperties = $this->getAllowedProperties($property->class)) {
            if (in_array($property->name, $allowedProperties)) {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * @param string $className
     * @return array|null
     */
    private function getAllowedProperties($className)
    {
        if (null === self::$allowedProperties) {
            self::$allowedProperties = [
                InvType::class => [
                    'typeID', 'groupID', 'typeName', 'volume', 'capacity', 'raceID',
                    'attributeDigest', 'marketGroup', 'description'
                ],
            ];
        }

        return isset(self::$allowedProperties[$className]) ? self::$allowedProperties[$className] : null;
    }
}