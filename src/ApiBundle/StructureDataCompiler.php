<?php

namespace GbsLogistics\PosFit\ApiBundle;

use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\AttributeCollectionDirective;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\ControlTowerResourcesDirective;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\GroupFetcherDirective;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\InvTypeFetcherDirective;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\MetaTypeFetcherDirective;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\ScaffoldingDirective;
use GbsLogistics\PosFit\ApiBundle\Model\StructureData;

class StructureDataCompiler
{
    /** @var GroupFetcherDirective */
    private $groupFetcherDirective;

    /** @var InvTypeFetcherDirective */
    private $invTypeFetcherDirective;

    /** @var AttributeCollectionDirective */
    private $attributeCollectionDirective;

    /** @var MetaTypeFetcherDirective */
    private $metaTypeFetcherDirective;

    /** @var ScaffoldingDirective */
    private $scaffoldingDirective;

    /** @var ControlTowerResourcesDirective */
    private $controlTowerResourcesDirective;

    function __construct(
        GroupFetcherDirective $groupFetcherDirective,
        InvTypeFetcherDirective $invTypeFetcherDirective,
        AttributeCollectionDirective $attributeCollectionDirective,
        MetaTypeFetcherDirective $metaTypeFetcherDirective,
        ScaffoldingDirective $scaffoldingDirective,
        ControlTowerResourcesDirective $controlTowerResourcesDirective
    ) {
        $this->groupFetcherDirective = $groupFetcherDirective;
        $this->invTypeFetcherDirective = $invTypeFetcherDirective;
        $this->attributeCollectionDirective = $attributeCollectionDirective;
        $this->metaTypeFetcherDirective = $metaTypeFetcherDirective;
        $this->scaffoldingDirective = $scaffoldingDirective;
        $this->controlTowerResourcesDirective = $controlTowerResourcesDirective;
    }

    /**
     * @param int $rootGroupId
     * @return StructureData
     */
    public function compileStructureData($rootGroupId)
    {
        $structureData = new StructureData();

        $structureData->setGroups($this->groupFetcherDirective->retrieveGroups($rootGroupId));
        $structureData->setInvTypes($this->invTypeFetcherDirective->retrieveInvTypes($structureData));
        $structureData->setAttributes($this->attributeCollectionDirective->collectDgmAttributeTypes($structureData));
        $structureData->setMetaTypes($this->metaTypeFetcherDirective->retrieveMetaTypes($structureData));
        $this->controlTowerResourcesDirective->retrieveControlTowerResources($structureData);
        $this->scaffoldingDirective->generateScaffolding($structureData);

        return $structureData;
    }
}