<?php

namespace GbsLogistics\PosFit\ApiBundle;

use GbsLogistics\PosFit\ApiBundle\DependencyInjection\ApiExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GbsLogisticsPosFitApiBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new ApiExtension();
    }

} 