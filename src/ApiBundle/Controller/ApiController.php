<?php

namespace GbsLogistics\PosFit\ApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    public function getDataAction()
    {
        $structureDataCache = $this->get('gbslogistics.pos_fit.api.structure_data_cache');
        $structureData = $structureDataCache->getStructureData();
        $response = new Response($structureData);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


}

