<?php


namespace GbsLogistics\PosFit\ApiBundle\Tests\Serialization;


use GbsLogistics\PosFit\ApiBundle\Model\StructureData;
use GbsLogistics\PosFit\DocumentBundle\Document\MarketGroup;
use GbsLogistics\SdeEntityBundle\Entity\DgmAttributeType;
use GbsLogistics\SdeEntityBundle\Entity\InvMetaType;
use GbsLogistics\SdeEntityBundle\Entity\InvType;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;

class StructureDataSerializationTest extends WebTestCase
{
    const FORMAT = 'json';

    /** @var SerializerInterface */
    private $serializer;

    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $this->serializer = $container->get('serializer');
    }

    public function testSerializeStructureData()
    {
        $structureData = new StructureData();
        $structureData->setInvTypes([new InvType() ]);
        $structureData->setMetaTypes([ new InvMetaType() ]);
        $structureData->setAttributes([ new DgmAttributeType() ]);
        $structureData->setGroups([ new MarketGroup(1, 2, 'A market group', 1, 2) ]);

        $json = $this->serializer->serialize($structureData, self::FORMAT);
        $retrievedData = $this->serializer->deserialize($json, StructureData::class, self::FORMAT);

        $this->assertEquals($structureData, $retrievedData);
    }
}
