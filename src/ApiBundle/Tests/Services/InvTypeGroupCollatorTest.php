<?php


namespace GbsLogistics\PosFit\ApiBundle\Tests\Services;


use GbsLogistics\PosFit\ApiBundle\Services\InvTypeGroupCollator;
use GbsLogistics\PosFit\ApiBundle\Model\InvTypeScaffolding;
use GbsLogistics\SdeEntityBundle\Entity\InvType;

class InvTypeGroupCollatorTest extends \PHPUnit_Framework_TestCase
{
    const TYPE_ID_1 = 123;
    const MARKET_GROUP_ID_1 = 456;
    const TYPE_ID_2 = 789;
    const TYPE_ID_3 = 5678;
    const MARKET_GROUP_ID_2 = 12345;

    /** @var InvTypeGroupCollator */
    private $sut;

    public function setUp()
    {
        $this->sut = new \GbsLogistics\PosFit\ApiBundle\Services\InvTypeGroupCollator();
    }

    public function testCollateInvTypesByGroup()
    {
        $invType1 = new InvType();
        $invType1->setTypeID(self::TYPE_ID_1);
        $invType1->setMarketGroupID(self::MARKET_GROUP_ID_1);

        $invType2 = new InvType();
        $invType2->setTypeID(self::TYPE_ID_2);
        $invType2->setMarketGroupID(self::MARKET_GROUP_ID_1);

        $invType3 = new InvType();
        $invType3->setTypeID(self::TYPE_ID_3);
        $invType3->setMarketGroupID(self::MARKET_GROUP_ID_2);

        $returnedArray = $this->sut->collateInvTypesByGroup([ $invType1, $invType2, $invType3 ]);
        $expectedArray = [
            self::MARKET_GROUP_ID_1 => [
                new InvTypeScaffolding(self::TYPE_ID_1),
                new InvTypeScaffolding(self::TYPE_ID_2),
            ],
            self::MARKET_GROUP_ID_2 => [
                new InvTypeScaffolding(self::TYPE_ID_3)
            ]
        ];

        $this->assertEquals($expectedArray, $returnedArray);
    }
}
