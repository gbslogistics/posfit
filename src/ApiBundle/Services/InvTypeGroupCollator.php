<?php

namespace GbsLogistics\PosFit\ApiBundle\Services;


use GbsLogistics\PosFit\ApiBundle\Model\InvTypeScaffolding;
use GbsLogistics\SdeEntityBundle\Entity\InvType;

class InvTypeGroupCollator
{
    public function collateInvTypesByGroup(array $invTypes)
    {
        $invTypesByGroup = [];

        /** @var InvType $invType */
        foreach ($invTypes as $invType) {
            $marketGroupId = $invType->getMarketGroupID();
            if (!isset($invTypesByGroup[$marketGroupId])) {
                $invTypesByGroup[$marketGroupId] = [];
            }

            $invTypesByGroup[$marketGroupId][] = new InvTypeScaffolding($invType);
        }

        return $invTypesByGroup;
    }
}