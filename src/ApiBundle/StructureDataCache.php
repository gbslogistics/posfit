<?php

namespace GbsLogistics\PosFit\ApiBundle;


use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;

class StructureDataCache implements CacheWarmerInterface
{
    const STARBASE_STRUCTURES_ROOT_GROUP_ID = 1285;
    const CACHE_DIRECTORY = '/posfit_api';
    const RELATIVE_CACHE_FILE_PATH = '/posfit_api/structure_data.json';

    /** @var StructureDataCompiler */
    private $structureDataCompiler;

    /** @var Serializer */
    private $serializer;

    /** @var string */
    private $cacheDir;

    function __construct(StructureDataCompiler $structureDataCompiler, Serializer $serializer, $cacheDir)
    {
        $this->structureDataCompiler = $structureDataCompiler;
        $this->serializer = $serializer;
        $this->cacheDir = $cacheDir;
    }

    public function getStructureData()
    {
        $filename = $this->cacheDir . self::RELATIVE_CACHE_FILE_PATH;

        if (is_file($filename)) {
            return file_get_contents($filename);
        }

        return $this->fetchAndSerializeData();
    }

    /**
     * Checks whether this warmer is optional or not.
     *
     * Optional warmers can be ignored on certain conditions.
     *
     * A warmer should return true if the cache can be
     * generated incrementally and on-demand.
     *
     * @return bool true if the warmer is optional, false otherwise
     */
    public function isOptional()
    {
        return false;
    }

    public function warmUp($cacheDir)
    {
        $serializedData = $this->fetchAndSerializeData();
        if (!is_dir($cacheDir . self::CACHE_DIRECTORY)) {
            mkdir($cacheDir . self::CACHE_DIRECTORY);
        }
        file_put_contents($cacheDir . self::RELATIVE_CACHE_FILE_PATH, $serializedData);
    }

    private function fetchAndSerializeData()
    {
        $structureData = $this->structureDataCompiler->compileStructureData(self::STARBASE_STRUCTURES_ROOT_GROUP_ID);
        return $this->serializer->serialize($structureData, 'json', $this->getContext());
    }

    private function getContext()
    {
        $context = new SerializationContext();
        $context->addExclusionStrategy(new PosFitExclusionStrategy());
        return $context;
    }

}