<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives;


use Doctrine\ORM\EntityRepository;
use GbsLogistics\PosFit\ApiBundle\Model\StructureData;
use GbsLogistics\SdeEntityBundle\Entity\InvMetaType;
use GbsLogistics\SdeEntityBundle\Entity\InvType;

class MetaTypeFetcherDirective
{
    /** @var EntityRepository */
    private $invMetaTypeRepository;

    function __construct(EntityRepository $invMetaTypeRepository)
    {
        $this->invMetaTypeRepository = $invMetaTypeRepository;
    }

    /**
     * @param StructureData $structureData
     * @return array
     */
    public function retrieveMetaTypes(StructureData $structureData)
    {
        $metaTypes = [];
        $typeIds = array_map(function (InvType $invType) {
            return $invType->getTypeID();
        }, $structureData->getInvTypes());

        $q = $this->invMetaTypeRepository->createQueryBuilder('m')
            ->where('m.typeID IN (:typeIDs)')
            ->setParameter('typeIDs', $typeIds);

        $metaTypesArray = $q->getQuery()->execute();
        /** @var InvMetaType $metaType */
        foreach ($metaTypesArray as $metaType) {
            $metaTypes[$metaType->getTypeID()] = $metaType;
        }
        
        return $metaTypes;
    }
}