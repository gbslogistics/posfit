<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives\GroupFilters;


class SegregateControlTowersGroupFilter
{
    private $groupsToSegregate = [ 478 ];

    public function shouldSegregate($groupId)
    {
        return in_array($groupId, $this->groupsToSegregate);
    }
}