<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives\GroupFilters;


class BlacklistGroupFilter
{
    private $blacklistedGroupIds = [
        1285,
        1010
    ];

    public function shouldExclude($groupId)
    {
        return in_array($groupId, $this->blacklistedGroupIds);
    }
}