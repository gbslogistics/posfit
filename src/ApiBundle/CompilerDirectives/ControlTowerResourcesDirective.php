<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives;


use Doctrine\ORM\EntityRepository;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\GroupFilters\SegregateControlTowersGroupFilter;
use GbsLogistics\PosFit\ApiBundle\Model\ControlTowerResources;
use GbsLogistics\PosFit\ApiBundle\Model\StructureData;
use GbsLogistics\SdeEntityBundle\Entity\InvControlTowerResource;
use GbsLogistics\SdeEntityBundle\Entity\InvType;

class ControlTowerResourcesDirective
{
    /** @var EntityRepository */
    private $repository;

    /** @var SegregateControlTowersGroupFilter */
    private $controlTowerFilter;

    function __construct(EntityRepository $repository)
    {
        if (InvControlTowerResource::class !== ($className = $repository->getClassName())) {
            throw new \InvalidArgumentException(sprintf(
                'Expected repository passed to ControlTowerResourcesDirective to be for class "%s", but was instead for class "%s".',
                InvControlTowerResource::class,
                $className
            ));
        }

        $this->repository = $repository;
        $this->controlTowerFilter = new SegregateControlTowersGroupFilter();
    }

    public function retrieveControlTowerResources(StructureData $structureData)
    {
        $resources = [];
        $controlTowerIds = array_filter(array_map(function (InvType $invType) {
            if ($this->controlTowerFilter->shouldSegregate($invType->getMarketGroupID())) {
                return $invType->getTypeID();
            }

            return false;
        }, $structureData->getInvTypes()));

        $resourceObjects = $this->repository->createQueryBuilder('r')
            ->where('r.controlTowerTypeID IN (:typeIds)')
            ->andWhere('r.factionID IS NULL')
            ->setParameter('typeIds', $controlTowerIds)
            ->getQuery()->execute();

        /** @var InvControlTowerResource $resourceObject */
        foreach ($resourceObjects as $resourceObject) {
            $controlTowerId = $resourceObject->getControlTowerTypeID();
            if (!isset($resources[$controlTowerId])) {
                $resources[$controlTowerId] = $resource = new ControlTowerResources($controlTowerId);
                $structureData->addControlTowerResource($resource);
            } else {
                $resource = $resources[$controlTowerId];
            }

            // Remove some of the unnecessary data
            $invType = $resourceObject->getResource();
            $invType->setDescription(null);
            $invType->setMarketGroup(null);

            if ('Online' === $resourceObject->getPurpose()->getPurposeText()) {
                $resource->setFuel($invType);
                $resource->setFuelPerHour($resourceObject->getQuantity());
            } elseif ('Reinforce' === $resourceObject->getPurpose()->getPurposeText()) {
                $resource->setReinforceFuel($invType);
                $resource->setReinforcePerHour($resourceObject->getQuantity());
            }
        }
    }
}