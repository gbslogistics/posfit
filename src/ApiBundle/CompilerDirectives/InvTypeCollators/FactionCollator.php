<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives\InvTypeCollators;


use GbsLogistics\PosFit\ApiBundle\Model\InvTypeCollection;
use GbsLogistics\PosFit\ApiBundle\Model\InvTypeScaffolding;
use GbsLogistics\PosFit\ApiBundle\Model\StructureData;
use GbsLogistics\SdeEntityBundle\Entity\InvMetaType;

class FactionCollator
{
    /**
     * @param array $invTypes
     * @param StructureData $structureData
     * @return bool
     */
    public function shouldCollate(array $invTypes, StructureData $structureData)
    {
        $metaTypes = $structureData->getMetaTypes();
        /** @var InvTypeScaffolding $invType */
        foreach ($invTypes as $invType) {
            if (isset($metaTypes[$invType->getTypeId()])) {
                return true;
            }
        }

        return false;
    }

    public function packInvTypes(array $invTypes, StructureData $structureData)
    {
        $factionTypes = [];
        $baseTypes = [];
        $metaTypes = $structureData->getMetaTypes();

        // Separate things that are a meta type from those that aren't
        /** @var InvTypeScaffolding $invType */
        foreach ($invTypes as $invType) {
            $typeId = $invType->getTypeId();
            if (isset($metaTypes[$typeId])) {
                /** @var InvMetaType $metaType */
                $metaType = $metaTypes[$typeId];
                $parentTypeId = $metaType->getParentTypeID();

                if (!isset($factionTypes[$parentTypeId])) {
                    $factionTypes[$parentTypeId] = [];
                }

                $factionTypes[$parentTypeId][] = $invType;
            } else {
                $baseTypes[$invType->getTypeId()] = $invType;
            }
        }

        // Insert faction types into their base type's collection
        /** @var InvTypeScaffolding $baseType */
        foreach ($baseTypes as $baseType) {
            $typeId = $baseType->getTypeId();
            if (isset($factionTypes[$typeId])) {
                if (null === ($collection = $baseType->getChildren())) {
                    $collection = new InvTypeCollection();
                    $collection->setSubType('faction');
                    $collection->setLabel(sprintf('Faction %s', $baseType->getGroupName()));
                    $baseType->setChildren($collection);
                }

                $collection->setInvTypes($factionTypes[$typeId]);
            }
        }

        return $baseTypes;
    }
}