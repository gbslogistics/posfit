<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives\InvTypeCollators;


use GbsLogistics\PosFit\ApiBundle\Model\InvTypeCollection;
use GbsLogistics\PosFit\ApiBundle\Model\InvTypeScaffolding;

class SizeCollator
{
    private static $chargeGroupSizeLabels = [
        1 => 'Small',
        2 => 'Medium',
        3 => 'Large',
        4 => 'X-Large'
    ];
    const CHARGE_SIZE = 'chargeSize';
    const AOE_CLOUD = 'missileEntityAoeCloudSizeMultiplier';
    const CONTROL_TOWER_SIZE = 'controlTowerSize';

    /**
     * @param array $invTypeScaffoldingArray
     * @return bool
     */
    public function shouldCollate(array $invTypeScaffoldingArray)
    {
        /** @var InvTypeScaffolding $scaffolding */
        foreach ($invTypeScaffoldingArray as $scaffolding) {
            $attributeDigest = $scaffolding->getInvType()->getAttributeDigest();
            if (!(
                isset($attributeDigest[self::CHARGE_SIZE])
                || isset($attributeDigest[self::AOE_CLOUD])
                || isset($attributeDigest[self::CONTROL_TOWER_SIZE])
            )) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $invTypeScaffoldingArray
     * @return SizeCollatorResults
     */
    public function collate(array $invTypeScaffoldingArray)
    {
        $invTypeCollections = [];
        $rejects = [];

        /** @var InvTypeScaffolding $scaffolding */
        foreach ($invTypeScaffoldingArray as $scaffolding) {
            $attributeDigest = $scaffolding->getInvType()->getAttributeDigest();
            if (isset($attributeDigest[self::CHARGE_SIZE])) {
                $chargeSize = $this->getSizeFromChargeSize($attributeDigest[self::CHARGE_SIZE]);
            }  elseif (isset($attributeDigest[self::AOE_CLOUD])) {
                $chargeSize = $this->getSizeFromAoeCloudSize($attributeDigest[self::AOE_CLOUD]);
            } elseif (isset($attributeDigest[self::CONTROL_TOWER_SIZE])) {
                $chargeSize = $this->getSizeFromChargeSize($attributeDigest[self::CONTROL_TOWER_SIZE]);
            } else {
                $rejects[] = $scaffolding;
                continue;
            }

            if (!isset($invTypeCollections[$chargeSize])) {
                $invTypeCollection = new InvTypeCollection();
                $invTypeCollection->setLabel($chargeSize);
                $invTypeCollection->setSubType('size');
                $invTypeCollections[$chargeSize] = $invTypeCollection;
            } else {
                /** @var InvTypeCollection $invTypeCollection */
                $invTypeCollection = $invTypeCollections[$chargeSize];
            }

            $invTypeCollection->addScaffolding($scaffolding);
        }

        return new SizeCollatorResults(array_reverse(array_values($invTypeCollections)), $rejects);
    }

    private function getSizeFromChargeSize($chargeSize)
    {
        return self::$chargeGroupSizeLabels[$chargeSize];
    }

    private function getSizeFromAoeCloudSize($aoeCloudSize)
    {
        if ($aoeCloudSize < 1) {
            return 'Large';
        }

        return 'X-Large';
    }
}