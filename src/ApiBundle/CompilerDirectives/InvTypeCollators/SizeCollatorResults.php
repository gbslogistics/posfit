<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives\InvTypeCollators;


class SizeCollatorResults
{
    /** @var array */
    private $invTypeCollections;
    /** @var array */
    private $rejects;

    function __construct($invTypeCollections, $rejects)
    {
        $this->invTypeCollections = $invTypeCollections;
        $this->rejects = $rejects;
    }

    /**
     * @return array
     */
    public function getInvTypeCollections()
    {
        return $this->invTypeCollections;
    }

    /**
     * @return array
     */
    public function getRejects()
    {
        return $this->rejects;
    }

}