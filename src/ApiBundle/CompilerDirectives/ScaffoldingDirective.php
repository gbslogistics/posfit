<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives;


use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\GroupFilters\BlacklistGroupFilter;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\GroupFilters\SegregateControlTowersGroupFilter;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\InvTypeCollators\FactionCollator;
use GbsLogistics\PosFit\ApiBundle\CompilerDirectives\InvTypeCollators\SizeCollator;
use GbsLogistics\PosFit\ApiBundle\Model\InvTypeCollection;
use GbsLogistics\PosFit\ApiBundle\Model\InvTypeScaffolding;
use GbsLogistics\PosFit\ApiBundle\Services\InvTypeGroupCollator;
use GbsLogistics\PosFit\ApiBundle\Model\GroupScaffolding;
use GbsLogistics\PosFit\ApiBundle\Model\StructureData;
use GbsLogistics\PosFit\DocumentBundle\Document\MarketGroup;

class ScaffoldingDirective
{
    /** @var \GbsLogistics\PosFit\ApiBundle\Services\InvTypeGroupCollator */
    private $invTypeGroupCollator;
    /** @var BlacklistGroupFilter */
    private $blacklistGroupFilter;
    /** @var SegregateControlTowersGroupFilter */
    private $segregateControlTowersGroupFilter;
    /** @var SizeCollator */
    private $sizeCollator;
    /** @var FactionCollator */
    private $factionCollator;

    function __construct() {
        $this->invTypeGroupCollator = new InvTypeGroupCollator();
        $this->blacklistGroupFilter = new BlacklistGroupFilter();
        $this->segregateControlTowersGroupFilter = new SegregateControlTowersGroupFilter();
        $this->sizeCollator = new SizeCollator();
        $this->factionCollator = new FactionCollator();
    }

    /**
     * @param StructureData $structureData
     * @return array
     */
    public function generateScaffolding(StructureData $structureData)
    {
        $invTypesByGroup = $this->invTypeGroupCollator->collateInvTypesByGroup($structureData->getInvTypes());
        $groupScaffoldingMap = [];
        $rootScaffoldingArray = [];
        $controlTowersGroup = [];
        $lastLeft = -1;
        $sortInvTypeScaffolding = function (InvTypeScaffolding $invTypeA, InvTypeScaffolding $invTypeB) {
            return strcmp($invTypeA->getTypeName(), $invTypeB->getTypeName());
        };

        /** @var MarketGroup $group */
        foreach ($structureData->getGroups() as $group) {
            if ($lastLeft > $group->getLeft()) {
                throw new \InvalidArgumentException(
                    'generateScaffolding expects groups to be ordered by their "left" attribute, ascending.'
                );
            }
            $lastLeft = $group->getLeft();

            $groupId = $group->getMarketGroupId();

            if ($this->blacklistGroupFilter->shouldExclude($groupId)) {
                continue;
            }

            $parentGroupId = $group->getParentGroupId();
            $collection = new GroupScaffolding($groupId, $group->getGroupName());
            $groupScaffoldingMap[$groupId] = $collection;

            if ($this->segregateControlTowersGroupFilter->shouldSegregate($groupId)) {
                $controlTowersGroup = $collection;
            } elseif (isset($groupScaffoldingMap[$parentGroupId])) {
                /** @var GroupScaffolding $parentScaffolding */
                $parentScaffolding = $groupScaffoldingMap[$parentGroupId];
                $parentScaffolding->addGroup($collection);
            } else {
                $rootScaffoldingArray[] = $collection;
            }

            if (isset($invTypesByGroup[$groupId])) {
                $invTypes = $invTypesByGroup[$groupId];

                if ($this->factionCollator->shouldCollate($invTypes, $structureData)) {
                    $invTypes = $this->factionCollator->packInvTypes($invTypes, $structureData);
                }

                if ($this->sizeCollator->shouldCollate($invTypes)) {
                    $collatedResults = $this->sizeCollator->collate($invTypes);
                    $invTypes = $collatedResults->getRejects();
                    $collection->setInvTypeCollections($collatedResults->getInvTypeCollections());
                }

                usort($invTypes, $sortInvTypeScaffolding);
                $collection->setInvTypes(array_values($invTypes));
            }
        }

        $structureData->setScaffolding($rootScaffoldingArray);

        // Sort control tower invTypes
        /** @var InvTypeCollection $collection */
        foreach ($controlTowersGroup->getInvTypeCollections() as $collection) {
            $invTypes = $collection->getInvTypes();
            usort($invTypes, $sortInvTypeScaffolding);
            $collection->setInvTypes($invTypes);
        }
        $structureData->setControlTowers($controlTowersGroup);
    }
}