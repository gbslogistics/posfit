<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives;


use Doctrine\ORM\EntityRepository;
use GbsLogistics\PosFit\ApiBundle\Model\StructureData;
use GbsLogistics\PosFit\DocumentBundle\Document\MarketGroup;
use GbsLogistics\SdeEntityBundle\Entity\InvType;

class InvTypeFetcherDirective
{
    /** @var EntityRepository */
    private $invTypesRepository;

    function __construct(EntityRepository $invTypesRepository)
    {
        $this->invTypesRepository = $invTypesRepository;
    }

    public function retrieveInvTypes(StructureData $structureData)
    {
        $groupIds = array_map(function (MarketGroup $group) {
            return (int)$group->getMarketGroupId();
        }, $structureData->getGroups());

        $q = $this->invTypesRepository->createQueryBuilder('i')
            ->where('i.marketGroupID IN (:marketGroupIDs)')
            ->andWhere('i.published = 1')
            ->setParameter('marketGroupIDs', $groupIds);

        $invTypesArray = $q->getQuery()->execute();
        $invTypes = [];
        /** @var InvType $invType */
        foreach ($invTypesArray as $invType) {
            $invTypes[$invType->getTypeID()] = $invType;
        }

        return $invTypes;
    }
}