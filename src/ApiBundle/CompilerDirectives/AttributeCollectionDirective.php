<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives;


use GbsLogistics\PosFit\ApiBundle\Model\StructureData;
use GbsLogistics\SdeEntityBundle\Entity\DgmTypeAttribute;
use GbsLogistics\SdeEntityBundle\Entity\InvType;

class AttributeCollectionDirective
{
    public function collectDgmAttributeTypes(StructureData $structureData)
    {
        // TODO: Replace with Set
        $attributes = [];

        /** @var InvType $invType */
        foreach ($structureData->getInvTypes() as $invType) {
            /** @var DgmTypeAttribute $attribute */
            foreach ($invType->getAttributes() as $attribute) {
                $attributeType = $attribute->getAttribute();
                $hash = $attributeType->getAttributeName();

                if (!isset($attributes[$hash])) {
                    $attributes[$hash] = $attributeType;
                }
            }
        }

        return $attributes;
    }
}