<?php

namespace GbsLogistics\PosFit\ApiBundle\CompilerDirectives;


use GbsLogistics\PosFit\DocumentBundle\Document\MarketGroup;
use GbsLogistics\PosFit\DocumentBundle\MarketGroupTransformer;

class GroupFetcherDirective
{
    /** @var MarketGroupTransformer */
    private $transformer;

    function __construct(MarketGroupTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param int $rootGroupId
     * @return array
     */
    public function retrieveGroups($rootGroupId)
    {
        $this->transformer->transform();
        $groups = [];
        foreach($this->transformer->getGroupAndChildren($rootGroupId) as $group) {
            $groups[$group->getMarketGroupId()] = $group;
        }

        return $groups;
    }
}