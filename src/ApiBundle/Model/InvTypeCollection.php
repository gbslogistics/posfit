<?php

namespace GbsLogistics\PosFit\ApiBundle\Model;


class InvTypeCollection
{
    /** @var string */
    private $type = "invTypeCollection";
    /** @var string */
    private $subType;
    /** @var string */
    private $label;
    /** @var array */
    private $invTypes = [];

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSubType()
    {
        return $this->subType;
    }

    /**
     * @param string $subType
     */
    public function setSubType($subType)
    {
        $this->subType = $subType;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return array
     */
    public function getInvTypes()
    {
        return $this->invTypes;
    }

    /**
     * @param array $invTypes
     */
    public function setInvTypes($invTypes)
    {
        $this->invTypes = $invTypes;
    }

    public function addScaffolding($scaffolding)
    {
        $this->invTypes[] = $scaffolding;
    }
}