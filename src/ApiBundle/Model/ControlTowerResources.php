<?php

namespace GbsLogistics\PosFit\ApiBundle\Model;


use GbsLogistics\SdeEntityBundle\Entity\InvType;

class ControlTowerResources
{
    /** @var integer */
    private $controlTowerTypeId;
    /** @var InvType */
    private $fuel;
    /** @var integer */
    private $fuelPerHour;
    /** @var InvType */
    private $reinforceFuel;
    /** @var integer */
    private $reinforcePerHour;

    function __construct($controlTowerTypeId)
    {
        $this->controlTowerTypeId = $controlTowerTypeId;
    }

    /**
     * @return int
     */
    public function getControlTowerTypeId()
    {
        return $this->controlTowerTypeId;
    }

    /**
     * @param int $controlTowerTypeId
     */
    public function setControlTowerTypeId($controlTowerTypeId)
    {
        $this->controlTowerTypeId = $controlTowerTypeId;
    }

    /**
     * @return InvType
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @param InvType $fuel
     */
    public function setFuel(InvType $fuel)
    {
        $this->fuel = $fuel;
    }

    /**
     * @return int
     */
    public function getFuelPerHour()
    {
        return $this->fuelPerHour;
    }

    /**
     * @param int $fuelPerHour
     */
    public function setFuelPerHour($fuelPerHour)
    {
        $this->fuelPerHour = $fuelPerHour;
    }

    /**
     * @return InvType
     */
    public function getReinforceFuel()
    {
        return $this->reinforceFuel;
    }

    /**
     * @param InvType $reinforceFuel
     */
    public function setReinforceFuel(InvType $reinforceFuel)
    {
        $this->reinforceFuel = $reinforceFuel;
    }

    /**
     * @return int
     */
    public function getReinforcePerHour()
    {
        return $this->reinforcePerHour;
    }

    /**
     * @param int $reinforcePerHour
     */
    public function setReinforcePerHour($reinforcePerHour)
    {
        $this->reinforcePerHour = $reinforcePerHour;
    }
}