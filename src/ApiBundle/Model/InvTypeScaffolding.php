<?php

namespace GbsLogistics\PosFit\ApiBundle\Model;


use GbsLogistics\SdeEntityBundle\Entity\InvType;

class InvTypeScaffolding
{
    /** @var InvTypeCollection|null */
    private $children = null;
    /** @var InvType */
    private $invType;

    function __construct(InvType $invType)
    {
        $this->invType = $invType;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->invType->getTypeID();
    }

    public function getTypeName()
    {
        return $this->invType->getTypeName();
    }

    public function getGroupName()
    {
        return $this->invType->getMarketGroup()->getMarketGroupName();
    }

    /**
     * @return InvTypeCollection|null
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param InvTypeCollection|null $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return InvType
     */
    public function getInvType()
    {
        return $this->invType;
    }
}