<?php

namespace GbsLogistics\PosFit\ApiBundle\Model;


use GbsLogistics\SdeEntityBundle\Entity\InvMetaType;

class StructureData
{
    /** @var array */
    private $invTypes = [];
    /** @var array */
    private $groups = [];
    /** @var array */
    private $attributes = [];
    /** @var array */
    private $metaTypes = [];
    /** @var array */
    private $scaffolding = [];
    /** @var array */
    private $controlTowers = [];
    /** @var array */
    private $controlTowerResources = [];

    /**
     * @return array
     */
    public function getInvTypes()
    {
        return $this->invTypes;
    }

    /**
     * @param array $invTypes
     */
    public function setInvTypes($invTypes)
    {
        $this->invTypes = $invTypes;
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param array $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return array
     */
    public function getMetaTypes()
    {
        return $this->metaTypes;
    }

    /**
     * @param array $metaTypes
     */
    public function setMetaTypes($metaTypes)
    {
        $this->metaTypes = $metaTypes;
    }

    /**
     * @return array
     */
    public function getScaffolding()
    {
        return $this->scaffolding;
    }

    /**
     * @param array $scaffolding
     */
    public function setScaffolding($scaffolding)
    {
        $this->scaffolding = $scaffolding;
    }

    /**
     * @return array
     */
    public function getControlTowers()
    {
        return $this->controlTowers;
    }

    /**
     * @param GroupScaffolding $controlTowers
     */
    public function setControlTowers($controlTowers)
    {
        $this->controlTowers = $controlTowers;
    }

    /**
     * @param $metaTypeId
     * @return InvMetaType|null
     */
    public function getSingleMetaType($metaTypeId)
    {
        return isset($this->metaTypes[$metaTypeId]) ? $this->metaTypes[$metaTypeId] : null;
    }

    /**
     * @return array
     */
    public function getControlTowerResources()
    {
        return $this->controlTowerResources;
    }

    /**
     * @param array $controlTowerResources
     */
    public function setControlTowerResources($controlTowerResources)
    {
        $this->controlTowerResources = $controlTowerResources;
    }

    public function addControlTowerResource(ControlTowerResources $controlTowerResources)
    {
        $this->controlTowerResources[$controlTowerResources->getControlTowerTypeId()] = $controlTowerResources;
    }
}