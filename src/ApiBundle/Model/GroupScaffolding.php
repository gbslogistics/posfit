<?php

namespace GbsLogistics\PosFit\ApiBundle\Model;


class GroupScaffolding
{
    /** @var string */
    private $type = "groupScaffolding";
    /** @var integer */
    private $groupId;
    /** @var string */
    private $name;
    /** @var array */
    private $childGroups = [];
    /** @var array */
    private $invTypes = [];
    /** @var array */
    private $invTypeCollections = [];

    /**
     * @param $groupId
     * @param $name
     */
    function __construct($groupId, $name)
    {
        $this->groupId = $groupId;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return array
     */
    public function getChildGroups()
    {
        return $this->childGroups;
    }

    /**
     * @param array $childGroups
     */
    public function setChildGroups($childGroups)
    {
        $this->childGroups = $childGroups;
    }

    /**
     * @return array
     */
    public function getInvTypes()
    {
        return $this->invTypes;
    }

    /**
     * @param array $invTypes
     */
    public function setInvTypes($invTypes)
    {
        $this->invTypes = $invTypes;
    }

    public function addGroup(GroupScaffolding $scaffolding)
    {
        $this->childGroups[] = $scaffolding;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getInvTypeCollections()
    {
        return $this->invTypeCollections;
    }

    /**
     * @param array $invTypeCollections
     */
    public function setInvTypeCollections($invTypeCollections)
    {
        $this->invTypeCollections = $invTypeCollections;
    }
}