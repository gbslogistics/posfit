<?php

namespace GbsLogistics\PosFit\DocumentBundle;

use Doctrine\ORM\EntityRepository;
use GbsLogistics\PosFit\DocumentBundle\Document\MarketGroup;
use GbsLogistics\PosFit\DocumentBundle\Model\MarketGroupGenealogy;
use GbsLogistics\SdeEntityBundle\Entity\InvMarketGroup;


/**
 * Uses the SDE to build a parent/child map of InvMarketGroups.
 *
 * @author Querns <querns@gbs.io>
 */
class MarketGroupGenealogist
{

    /** @var \Doctrine\ORM\EntityRepository */
    private $repository;

    function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;

        $invMarketGroupClass = InvMarketGroup::class;
        $repositoryClassName = $repository->getClassName();

        if ($invMarketGroupClass !== $repositoryClassName) {
            throw new \InvalidArgumentException(sprintf(
                'MarketGroupGenealogist expects to be built with a repository for class "%s", got a repository for class "%s" instead.',
                $invMarketGroupClass,
                $repositoryClassName
            ));
        }
    }
    /**
     * @return MarketGroupGenealogy
     */
    public function getGenealogy()
    {
        $roots = [];
        $familyTree = [];
        $names = [];

        /** @var InvMarketGroup $invMarketGroup */
        foreach ($this->repository->findAll() as $invMarketGroup) {
            $parentId = $invMarketGroup->getParentGroupID();
            $childId = $invMarketGroup->getMarketGroupID();
            $name = $invMarketGroup->getMarketGroupName();

            $names[$childId] = $name;

            if (null === $parentId) {
                $roots[] = $childId;
            } else {
                if (!array_key_exists($parentId, $familyTree)) {
                    $familyTree[$parentId] = [];
                }

                $familyTree[$parentId][] = $childId;
            }
        }

        return new MarketGroupGenealogy($roots, $familyTree, $names);
    }
}
