<?php

namespace GbsLogistics\PosFit\DocumentBundle\Document;


class MarketGroup
{
    /** @var string */
    private $id;

    /** @var integer */
    private $marketGroupId;

    /** @var integer */
    private $parentGroupId;

    /** @var integer */
    private $left;

    /** @var integer */
    private $right;

    /** @var string */
    private $groupName;

    function __construct($marketGroupId, $parentGroupId, $groupName, $left, $right)
    {
        $this->marketGroupId = $marketGroupId;
        $this->groupName = $groupName;
        $this->left = $left;
        $this->right = $right;
        $this->parentGroupId = $parentGroupId;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @param string $groupName
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;
    }

    /**
     * @return int
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * @param int $left
     */
    public function setLeft($left)
    {
        $this->left = $left;
    }

    /**
     * @return int
     */
    public function getMarketGroupId()
    {
        return $this->marketGroupId;
    }

    /**
     * @param int $marketGroupId
     */
    public function setMarketGroupId($marketGroupId)
    {
        $this->marketGroupId = $marketGroupId;
    }

    /**
     * @return int
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * @param int $right
     */
    public function setRight($right)
    {
        $this->right = $right;
    }

    /**
     * @return int
     */
    public function getParentGroupId()
    {
        return $this->parentGroupId;
    }

    /**
     * @param int $parentGroupId
     */
    public function setParentGroupId($parentGroupId)
    {
        $this->parentGroupId = $parentGroupId;
    }
}