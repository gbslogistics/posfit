<?php

namespace GbsLogistics\PosFit\DocumentBundle;

use Doctrine\ODM\MongoDB\DocumentManager;
use GbsLogistics\PosFit\DocumentBundle\Document\MarketGroup;
use GbsLogistics\PosFit\DocumentBundle\Model\MarketGroupGenealogy;


/**
 * @author Querns <querns@gbs.io>
 */
class MarketGroupTransformer
{
    /** @var array */
    protected $lineage = [];

    /** @var MarketGroupGenealogist */
    private $genealogist;

    /** @var MarketGroup[] */
    private $groupStorage = [];

    function __construct(MarketGroupGenealogist $genealogist)
    {
        $this->genealogist = $genealogist;
    }

    public function transform()
    {
        $genealogy = $this->genealogist->getGenealogy();
        $index = 1;

        foreach ($genealogy->getRootGroupIds() as $groupId) {
            $this->traverseLineage($genealogy, $groupId, null, $index);
        }
    }

    /**
     * @param $rootGroupId
     * @return MarketGroup[]
     */
    public function getGroupAndChildren($rootGroupId)
    {
        if (!isset($this->groupStorage[$rootGroupId])) {
            return [];
        }

        $rootGroup = $this->groupStorage[$rootGroupId];
        /** @var MarketGroup[] $validGroups */
        $validGroups = [];

        foreach ($this->groupStorage as $groupId => $marketGroup) {
            if ($marketGroup->getLeft() >= $rootGroup->getLeft() && $marketGroup->getRight() <= $rootGroup->getRight()) {
                $validGroups[] = $marketGroup;
            }
        }

        usort($validGroups, function (MarketGroup $a, MarketGroup $b) {
            return $a->getLeft() - $b->getLeft();
        });

        return $validGroups;
    }

    protected function traverseLineage(MarketGroupGenealogy $genealogy, $groupId, $parentGroupId, &$index)
    {
        $left = $index;
        $index++;

        foreach ($genealogy->getChildren($groupId) as $childId) {
            $this->traverseLineage($genealogy, $childId, $groupId, $index);
        }

        $right = $index;
        $index++;
        $this->persistMarketGroupLineage($groupId, $parentGroupId, $left, $right, $genealogy->getGroupName($groupId));
    }

    protected function persistMarketGroupLineage($groupId, $parentGroupId, $left, $right, $name)
    {
        $this->groupStorage[$groupId] = new MarketGroup($groupId, $parentGroupId, $name, $left, $right);
    }
}

