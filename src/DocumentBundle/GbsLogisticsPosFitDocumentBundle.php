<?php

namespace GbsLogistics\PosFit\DocumentBundle;


use GbsLogistics\PosFit\DocumentBundle\DependencyInjection\ContainerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GbsLogisticsPosFitDocumentBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new ContainerExtension();
    }

}