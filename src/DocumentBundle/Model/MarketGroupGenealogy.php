<?php

namespace GbsLogistics\PosFit\DocumentBundle\Model;


/**
 * Holds information about market group descendants and ancestors.
 *
 * @author Querns <querns@gbs.io>
 */
class MarketGroupGenealogy
{
    /** @var array */
    protected $rootGroupIds = [];

    /** @var array */
    protected $familyTree = [];

    /** @var array */
    private $names = [];

    /**
     * @param $rootGroupIds
     * @param $familyTree
     * @param $names
     */
    function __construct($rootGroupIds, $familyTree, $names)
    {
        $this->rootGroupIds = $rootGroupIds;
        $this->familyTree = $familyTree;
        $this->names = $names;
    }

    /**
     * @return array
     */
    public function getRootGroupIds()
    {
        return $this->rootGroupIds;
    }

    /**
     * @param $groupId
     * @return array
     */
    public function getChildren($groupId)
    {
        if (isset($this->familyTree[$groupId])) {
            return $this->familyTree[$groupId];
        }

        return [];
    }

    /**
     * @param $groupId
     * @return string|null
     */
    public function getGroupName($groupId)
    {
        if (isset($this->names[$groupId])) {
            return $this->names[$groupId];
        }

        return null;
    }
}

